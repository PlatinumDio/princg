#ifndef MAT4_H
#define MAT4_H
#include "Vec4.h"

class Mat4
{
public:
	float m[4][4];
	Mat4();
	~Mat4();
	Mat4 Perpective(float FOV, float aspect, float near, float far);
	static Mat4 Identity();
	static Mat4 RotateX(const float& angle);
	static Mat4 LookAt(Vec4 eye, Vec4 target, Vec4 up);
	Mat4 operator*(const Mat4 & rh)const;
	Vec4 operator*(const Vec4 & rh)const;
	static Mat4 RotateY(const float& angle);
	static Mat4 RotateZ(const float& angle);
	static Mat4 Translate(const float& Tx, const float& Ty, const float& Tz);

	static Mat4 Translate(const Vec4& v);
	//static Mat4& Scale(const Vec4& v);
	
};
#endif

