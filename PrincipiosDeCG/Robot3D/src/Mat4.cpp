#ifndef MAT4_CPP
#define MAT4_CPP
#define _USE_MATH_DEFINES
#include <math.h>
#include "Mat4.h"


Mat4::Mat4()
{
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			if (i == j) {
				m[i][j] = 1;
			}
			else {
				m[i][j] = 0;
			}
		}
	}
}

Mat4 Mat4::Identity() {
	Mat4 result;
	return result;
}

Mat4 Mat4::Translate(const float& Tx, const float& Ty, const float& Tz) {
	Mat4 m;
	m.m[0][0] = m.m[1][1] = m.m[2][2] = m.m[3][3]= 1;
	m.m[0][3] = Tx;
	m.m[1][3] = Ty;
	m.m[2][3] = Tz;
	m.m[0][1] = m.m[0][2] = m.m[1][0] = m.m[1][2] = m.m[2][0] = m.m[2][1] = m.m[3][0]= m.m[3][1] = m.m[3][2]= 0;
	return m;
}

Mat4 Mat4::Translate(const Vec4& v) {
	return(Translate(v.m[0], v.m[1], v.m[2]));
}
Mat4 Mat4::Scale(const float& Sx, const float& Sy, const float& Sz) {
	Mat4 m;
	m.m[0][0] = Sx;
	m.m[1][1] = Sy;
	m.m[2][2] = Sz;
	m.m[3][3] = 1;
	m.m[0][1] = m.m[0][2] = m.m[0][3] = m.m[1][0] = m.m[1][2] = m.m[1][3] = m.m[2][0] = m.m[2][1] = m.m[2][3] = m.m[3][0] = m.m[3][1] = m.m[3][2] = 0;
	return m;
}
 Mat4 Mat4::RotateZ(const float& angle) {
	 Mat4 m;
	 m.m[0][0] = m.m[1][1] = cos(angle*M_PI / 180);
	 m.m[1][0] = sin(angle*M_PI / 180);
	 m.m[0][1] = -sin(angle*M_PI / 180);
	 m.m[2][0] = m.m[2][1] = m.m[1][2] = m.m[0][2] = m.m[0][3] = m.m[1][3] = m.m[2][3] = m.m[3][0] = m.m[3][1] = m.m[3][2] = 0;
	 m.m[2][2] = m.m[3][3] = 1;
	 return m;
}

 Mat4 Mat4:: RotateY(const float& angle) {
	 Mat4 m;
	 m.m[0][0] = m.m[2][2] = cos(angle*M_PI / 180);
	 m.m[2][0] = -sin(angle*M_PI / 180);
	 m.m[0][2] = sin(angle*M_PI / 180);
	 m.m[1][0] = m.m[2][1] = m.m[1][2] = m.m[0][1] = m.m[0][3] = m.m[1][3] = m.m[2][3] = m.m[3][0] = m.m[3][1] = m.m[3][2] = 0;
	 m.m[1][1] = m.m[3][3] = 1;
	 return m;
 }

 Mat4 Mat4::RotateX(const float& angle) {
	 Mat4 m;
	 m.m[1][1] = m.m[2][2] = cos(angle*M_PI / 180);
	 m.m[2][1] = sin(angle*M_PI / 180);
	 m.m[1][2] = -sin(angle*M_PI / 180);
	 m.m[1][0] = m.m[0][2] = m.m[2][0] = m.m[0][1] = m.m[0][3] = m.m[1][3] = m.m[2][3] = m.m[3][0] = m.m[3][1] = m.m[3][2] = 0;
	 m.m[0][0] = m.m[3][3] = 1;
	 return m;
 }

Mat4 Mat4::Perpective(float FOV, float aspect, float near, float far) {
	float cot = cosf(FOV * (M_PI / 180) / 2);
	cot = sinf(FOV* (M_PI/180)/2) / cot;
	cot = 1.0f / cot;

	Mat4 m;
	m.m[0][0] = cot / aspect;
	m.m[1][1] = cot;
	m.m[2][2] = (-far + near) / (far - near);
	m.m[2][3] = -(-2 * far * near) / (far - near);
	m.m[3][2] = 1;
	return m;

}

Mat4 Mat4::LookAt(Vec4 eye, Vec4 target, Vec4 up) {
	Vec4 Front = (target - eye).Normalize();
	Vec4 Upp = Front.Cross(up).Normalize();
	Vec4 Right = Upp.Cross(Front);

	Mat4 t;
	Mat4 f = f.Translate(-eye.x(), -eye.y(), -eye.z());
	t.m[0][0] = Upp.x();   
	t.m[0][1] = -Right.x();   
	t.m[0][2] = Front.x();

	t.m[1][0] = Upp.y();   
	t.m[1][1] = -Right.y();   
	t.m[1][2] = Front.y();

	t.m[2][0] = Upp.z();   
	t.m[2][1] = -Right.z();   
	t.m[2][2] = Front.z();

	t = t*f;
	return t;
}

Mat4 Mat4::operator*(const Mat4 & rh)const {
	Mat4 r;
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			r.m[i][j] = 0;
		}
	}
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			for (int k = 0; k < 4; ++k)
			{
				r.m[i][j] += m[i][k] * rh.m[k][j];
			}
		}
	}
	return r;
}

Vec4 Mat4::operator*(const Vec4 & rh)const {
	Vec4 r;
	for (int i = 0; i < 4; ++i) {
		r.m[i] = 0;
	}
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			r.m[i] += m[i][j] * rh.m[j];
		}
	}
	return r;
}

Mat4::~Mat4()
{
}
#endif