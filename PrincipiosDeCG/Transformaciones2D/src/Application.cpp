#include "stdafx.h"
#include "Application.h"
#include <vector>
#include "Mat3.h"
#include "BaseApplication.h"
using namespace std;
Mat3 mat;
void Application::setup() {
	heightCenter = HEIGHT / 2;
	widthCenter = WIDTH / 2;
	Syerpinzky(Vec3(widthCenter, heightCenter + 300, 1), Vec3(widthCenter + 300, heightCenter-300, 1),
		Vec3(widthCenter - 300, heightCenter-300, 1), 3);
}

void Application::update() {

	vc.clear();
	aang++;
	korra++;
	Mat3 acum = mat.Translate(widthCenter, heightCenter)* mat.Rotate(korra)*mat.Translate(150, 0) * mat.Rotate(aang)*mat.Translate(-widthCenter,- heightCenter);
	for (int i = 0; i < v.size(); ++i) {
		vc.push_back(acum* v[i]);
	}
}

void Application::moveTo(float x1, float y1) {
	x = x1;
	y = y1;

}

void Application::LineTo(Vec3& b) {
	drawLine(x,y, b.x(), b.y());
	moveTo(b.x(), b.y());
}

void Application::Triangles(Vec3 a, Vec3 b, Vec3 c){
	moveTo(a.x(), a.y());
	LineTo(b);
	LineTo(c);
	LineTo(a);
}

Vec3 Application::MidPoint(const Vec3 A, const Vec3 B) {
	Vec3 r;
	for (int i = 0; i < 3; ++i) {
		r.m[i] = (A.m[i] + B.m[i]) / 2;
	}
	return r;
}

void Application::Syerpinzky(Vec3 a, Vec3 b, Vec3 c, int subd) {

	if (subd >= 1) {
		Vec3 ab = MidPoint(a, b);
		Vec3 bc = MidPoint(b, c);
		Vec3 ca = MidPoint(c, a);
		Syerpinzky(a, ab, ca, subd - 1);
		Syerpinzky(ab, b, bc, subd - 1);
		Syerpinzky(ca, bc, c, subd - 1);
	}
	else {
		v.push_back(a);
		v.push_back(b);
		v.push_back(c);
	}

}

void Application::drawPolygon(int sides) {
	float angulo = 360 / sides;
	int x0, x1, y0, y1;
	int radio = 250;
	x0 = widthCenter + radio;
	y0 = heightCenter;
	for (int ang = 0; ang <= 360; ang += angulo) {
		x1 =radio * cos(ang*rad)+ widthCenter;
		y1 =radio * sin (ang * rad) + heightCenter;
		drawLine(x0, y0, x1, y1);
		x0 = x1;
		y0 = y1;
	}
}

void Application::draw() {
	//grad %= 360;
	//rotation += 2;
	//lados += (float)0.05;
	//if (rotation > 360) {
	//	rotation %= 360;
	//	lados -= 8;
	//}
	//drawLine(widthCenter, heightCenter,
	//	heightCenter + heightCenter * std::sin((rotation + grad) * radians),
	//	widthCenter + widthCenter * std::cos((rotation + grad) * radians)
	//);
	//drawPolygon(5);
	for (int i = 0; i < 1023; ++i) {
		for (int j = 0; j < 1023; ++j) {
			putPixel(i, j, 120, 120, 120, 255);
		}
	}

	
	for (int i = 0; i < vc.size(); i += 3) {
		Triangles(vc[i], vc[i + 1], vc[i + 2]);
	
	}
}




