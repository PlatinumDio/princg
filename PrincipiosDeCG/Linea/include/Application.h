#pragma once
#include "BaseApplication.h"
class Application :public BaseApplication
{
public:
	int grad = 0;
	float rad = (float) 3.1416/180;
	void setup() override;
	void update() override;
	void draw() override;
};
