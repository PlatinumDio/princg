#pragma once
#include "BaseApplication.h"
class Vec3; 
class Vec4;

using namespace std;
class Application :public BaseApplication
{
public:
	enum Tipo {
		TRIANGLES,
		POINTS,
		LINES,
		LINESTRIP,
		LINELOOP,
		TRIANGLESTRIP,
		TRIANGLEFAN
	};
	void drawGeometry(const std::vector<Vec3>& VT, Tipo tipo);
	float aang = 0, korra = 0, sum = 1 , zuko =0;
	float x, y;
	std::vector<Vec3> Geom;
	std::vector<Vec3> v;
	std::vector<Vec3> vc;
	std::vector<Vec4> V3;
	std::vector<Vec4> V2;
	std::vector<Vec4> Vcopy;
	std::vector<Vec4> Vcopy1;
	std::vector<Vec4> Vcopy2;
	std::vector<Vec4> Vcopy3;
	std::vector<Vec4> Vcopy4;
	std::vector<Vec4> Vcopy5;
	std::vector<Vec4> Vcopy6;
	std::vector<Vec4> Vcopy7;
	std::vector<Vec4> Vcopy8;
	std::vector<Vec4> Vcopy9;
	std::vector<Vec4> Vfin1;
	std::vector<Vec4> Vfin2;
	std::vector<Vec4> Vfin3;
	std::vector<Vec4> Vfin4;
	std::vector<Vec4> Vfin5;
	std::vector<Vec4> Vfin6;
	std::vector<Vec4> Vfin7;
	std::vector<Vec4> Vfin8;
	std::vector<Vec4> Vfin9;
	int heightCenter;
	int widthCenter;
	int grad = 0;
	int lados, radians, rotation;
	float rad = (float) 3.1416/180;
	void setup() override;
	void update() override;
	void draw() override;
	Vec3 MidPoint(Vec3 a, Vec3 b);
	void moveTo(float x1, float y1);
	void LineTo( Vec3& b);
	void LineTo(float x, float y);

	void MidPointCircle(int x, int y, int rad);

	void Triangles(Vec3 a, Vec3 b, Vec3 c);
	void drawPolygon(int sides);
	void Syerpinzky(Vec3 a, Vec3 b, Vec3 c, int subd);
};
