#ifndef VEC4_CPP
#define VEC4_CPP
#include "Vec4.h"
#include <math.h>
Vec4::Vec4()
{
	m[0] = 0;
	m[1] = 0;
	m[2] = 0;
	m[3] = 1;
}

Vec4::Vec4(const float& x, const float& y, const float& z) {
	m[0] = x;
	m[1] = y;
	m[2] = z;
	m[3] = 1;
}

Vec4& Vec4::Cross(const Vec4& c) const {
	Vec4 cross = Vec4();
	cross.m[0] = ((this->m[1] * c.m[2]) - (this->m[2] * c.m[1]));
	cross.m[1] = -((this->m[0] * c.m[2]) - (this->m[2] * c.m[0]));
	cross.m[2] = ((this->m[0] * c.m[1]) - (this->m[1] * c.m[0]));
	cross.m[3] = 1;
	return cross;
}

float Vec4::Dot(const Vec4& c) const {
	return((this->m[0]*c.m[0])+(this->m[1]*c.m[1])+(this->m[2]*c.m[2]));
}

Vec4&  Vec4::operator -(const Vec4& rh) const {
	return Vec4(this->m[0] - rh.m[0], this->m[1] - rh.m[1], this->m[2]-rh.m[2]);
}

Vec4 Vec4::Normalize() {
	float temp;
	Vec4 g = Vec4();
	temp = sqrt((this->x()*this->x()) + (this->y()*this->y()) + (this->z()*this->z()));
	for (int i = 0; i <= 3;++i) {
		g.m[i] = this->m[i] / temp;
	}
	g.m[3] = 1;
	return g;
	
}

Vec4::~Vec4()
{
}
#endif