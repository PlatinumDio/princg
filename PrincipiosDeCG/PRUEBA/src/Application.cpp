#pragma once
#include "Application.h"

void Application::draw() {
	for (int x = 0; x < WIDTH; x++) {
		putPixel(x, WIDTH / 2, 0, 255, 0, 255);
	}
	for (int i = 0; i < HEIGHT; i++) {
		putPixel(HEIGHT / 2, i, 0, 255, 0, 255);
	}
	for (int i = 0; i < HEIGHT; i++) {
		putPixel(i*-1, i, 0, 255, 0, 255);
	}
	for (int i = 0; i < HEIGHT; i++) {
		putPixel(i, i, 0, 255, 0, 255);
	}
}
void Application::setup() {

}
void Application::update() {

}