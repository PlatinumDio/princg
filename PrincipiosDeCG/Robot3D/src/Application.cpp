
#include "Application.h"
using namespace std;
Mat3 mat;
Mat4 matt;
float xPri, yPri, radd;

//Vcopy = vector<Vec4>();
void Application::setup() {
	heightCenter = HEIGHT / 2;
	widthCenter = WIDTH / 2;
    

	//Vec3 Tri1 = Vec3(0 + heightCenter, 0 + widthCenter , 1);
	//Vec3 Tri2 = Vec3(180 + heightCenter, 20 + widthCenter , 1);
	//Vec3 Tri3 = Vec3(260 + heightCenter, 140 + widthCenter , 1);
	//Vec3 Tri4 = Vec3(40 + heightCenter, 160 + widthCenter , 1);
	//Vec3 Tri5 = Vec3(160 + heightCenter, 40 + widthCenter , 1);
	//Vec3 Tri6 = Vec3(80 + heightCenter, 220 + widthCenter , 1);

	//Geom.push_back(Tri1);
	//Geom.push_back(Tri2);
	//Geom.push_back(Tri3);
	//Geom.push_back(Tri4);
	//Geom.push_back(Tri5);
	//Geom.push_back(Tri6);
	

	Vec4 d = Vec4(-100 + heightCenter, 100 + widthCenter, 100);
	Vec4 h = Vec4(-100 + heightCenter, 100 + widthCenter, -100);

	Vec4 a = Vec4(100 + heightCenter, 100 + widthCenter, 100);
	Vec4 e = Vec4(100 + heightCenter, 100 + widthCenter, -100);

	Vec4 c = Vec4(-100 + heightCenter, -100 + widthCenter, 100);
	Vec4 g = Vec4(-100 + heightCenter, -100 + widthCenter, -100);

	Vec4 b = Vec4(100 + heightCenter, -100 + widthCenter, 100);
	Vec4 f = Vec4(100 + heightCenter, -100 + widthCenter, -100);

	V3.push_back(a);
	V3.push_back(b);
	V3.push_back(d);

	V3.push_back(d);
	V3.push_back(c);
	V3.push_back(b);

	V3.push_back(a);
	V3.push_back(e);
	V3.push_back(h);

	V3.push_back(h);
	V3.push_back(d);
	V3.push_back(a);

	V3.push_back(e);
	V3.push_back(f);
	V3.push_back(g);

	V3.push_back(f);
	V3.push_back(g);
	V3.push_back(h);

	V3.push_back(f);
	V3.push_back(b);
	V3.push_back(c);

	V3.push_back(f);
	V3.push_back(g);
	V3.push_back(c);

	V3.push_back(a);
	V3.push_back(b);
	V3.push_back(f);

	V3.push_back(e);
	V3.push_back(f);
	V3.push_back(b);

	V3.push_back(h);
	V3.push_back(g);
	V3.push_back(c);

	V3.push_back(h);
	V3.push_back(c);
	V3.push_back(d);

	Vec4 d2 = Vec4(-50 + heightCenter, 50 + widthCenter, 50);
	Vec4 h2 = Vec4(-50 + heightCenter, 50 + widthCenter, -50);

	Vec4 a2 = Vec4(50 + heightCenter, 50 + widthCenter, 50);
	Vec4 e2 = Vec4(50 + heightCenter, 50 + widthCenter, -50);

	Vec4 c2 = Vec4(-50 + heightCenter, -50 + widthCenter, 50);
	Vec4 g2 = Vec4(-50 + heightCenter, -50 + widthCenter, -50);

	Vec4 b2 = Vec4(50 + heightCenter, -50 + widthCenter, 50);
	Vec4 f2 = Vec4(50 + heightCenter, -50 + widthCenter, -50);

	V2.push_back(a2);
	V2.push_back(b2);
	V2.push_back(d2);

	V2.push_back(d2);
	V2.push_back(c2);
	V2.push_back(b2);

	V2.push_back(a2);
	V2.push_back(e2);
	V2.push_back(h2);

	V2.push_back(h2);
	V2.push_back(d2);
	V2.push_back(a2);

	V2.push_back(e2);
	V2.push_back(f2);
	V2.push_back(g2);

	V2.push_back(f2);
	V2.push_back(g2);
	V2.push_back(h2);

	V2.push_back(f2);
	V2.push_back(b2);
	V2.push_back(c2);

	V2.push_back(f2);
	V2.push_back(g2);
	V2.push_back(c2);

	V2.push_back(a2);
	V2.push_back(b2);
	V2.push_back(f2);

	V2.push_back(e2);
	V2.push_back(f2);
	V2.push_back(b2);

	V2.push_back(h2);
	V2.push_back(g2);
	V2.push_back(c2);

	V2.push_back(h2);
	V2.push_back(c2);
	V2.push_back(d2);

   /*Syerpinzky(Vec3(widthCenter, heightCenter + 300, 1), Vec3(widthCenter + 300, heightCenter-300, 1),
		Vec3(widthCenter - 300, heightCenter-300, 1), 3);*/

	
}

void Application::update() {
	
	/*vc.clear();*/
	Vcopy.clear();
	Vcopy1.clear();
	Vcopy2.clear();
	Vfin1.clear();
	Vfin2.clear();
	Vfin3.clear();
	Vfin4.clear();
	Vfin5.clear();
	Vfin6.clear();
	Vfin7.clear();
	Vfin8.clear();
	Vcopy9.clear();
	aang +=sum;
	korra+=2;
	zuko--;
	if (aang >= 360|| aang<=-360) {
		sum*-1;
	}
	//sum = aang < 360  ? sum*-1 : sum;
	
	//Mat3 acum = mat.Translate(widthCenter, heightCenter)* mat.Rotate(korra)*mat.Translate(150, 0) * mat.Rotate(aang)*mat.Translate(-widthCenter,- heightCenter);
	//for (int i = 0; i < v.size(); ++i) {
	//	vc.push_back(acum* v[i]);
	//}


	
	Mat4 acum = /*matt.Perpective(0.3, 1, 0.01f, 10)*matt.LookAt(Vec4(10, 2, 10), Vec4(0, 0, 0),
		Vec4(0,1,0))**/matt.Translate(heightCenter, widthCenter, 0)*matt.RotateY(korra)*matt.RotateX(korra)*matt.Translate(-heightCenter, -widthCenter,0);

	Mat4 Brz1 = matt.Translate(heightCenter, widthCenter-50, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz1T = acum*matt.Translate(heightCenter+150, widthCenter, 0)*matt.RotateX(korra)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz2 = matt.Translate(heightCenter, widthCenter, 0)*matt.Translate(0, -100,0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz2T =Brz1T*matt.Translate(0, -150, 0)*matt.Translate(heightCenter,widthCenter, 0)*matt.RotateX(aang<-20? -20 :aang > 60? 60 : aang = aang)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz3 = matt.Translate(heightCenter, widthCenter - 50, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz3T = acum*matt.Translate(heightCenter - 150, widthCenter, 0)*matt.RotateX(korra)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz4 = matt.Translate(heightCenter, widthCenter, 0)*matt.Translate(0, -100, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Brz4T = Brz3T*matt.Translate(0, -150, 0)*matt.Translate(heightCenter, widthCenter, 0)*matt.RotateX(aang<-20 ? -20 : aang > 60 ? 60 : aang = aang)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Prn1 = matt.Translate(heightCenter, widthCenter, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);
	
	Mat4 Prn2 = matt.Translate(heightCenter, widthCenter, 0)*matt.Translate(0, -100, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);
	
	Mat4 Prn3 = matt.Translate(heightCenter, widthCenter, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Prn4 = matt.Translate(heightCenter, widthCenter, 0)*matt.Translate(0, -100, 0)*matt.Scale(1, 2, 1)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Prn1T = acum*matt.Translate(heightCenter + 50, widthCenter-150, 0)*matt.RotateX(aang<-20 ? -20 : aang > 60 ? 60 : aang = aang)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Prn2T = Prn1T*matt.Translate(0, -100, 0)*matt.Translate(heightCenter, widthCenter, 0)*matt.RotateX(zuko < -60 ? -60 : zuko > 60 ? 60 : zuko = zuko)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Prn3T = acum*matt.Translate(heightCenter - 50, widthCenter - 150, 0)*matt.RotateX(zuko < -60 ? -60 : zuko > 60 ? 60 : zuko = zuko)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Prn4T = Prn3T*matt.Translate(0, -100, 0)*matt.Translate(heightCenter, widthCenter, 0)*matt.RotateX(zuko < -60 ? -60 : zuko > 60 ? 60 : zuko = zuko)*matt.Translate(-heightCenter, -widthCenter, 0);

	Mat4 Head = acum* matt.Translate(heightCenter, widthCenter + 150, 0)*matt.RotateY(korra)*matt.Translate(-heightCenter, -widthCenter, 0);

	for (int i = 0; i < V3.size(); ++i) {
		Vcopy.push_back(acum* V3[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy1.push_back(Brz1* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy2.push_back(Brz2* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy3.push_back(Prn1* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy4.push_back(Prn2* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy5.push_back(Prn3* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy6.push_back(Prn4* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy7.push_back(Brz3* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy8.push_back(Brz4* V2[i]);
	}
	for (int i = 0; i < V2.size(); ++i) {
		Vcopy9.push_back(Head* V2[i]);
	}
	for (int i = 0; i < Vcopy1.size(); ++i) {
		Vfin1.push_back(Brz1T* Vcopy1[i]);
	}
	for (int i = 0; i < Vcopy2.size(); ++i) {
		Vfin2.push_back(Brz2T* Vcopy2[i]);
	}
	for (int i = 0; i < Vcopy3.size(); ++i) {
		Vfin3.push_back(Prn1T* Vcopy3[i]);
	}
	for (int i = 0; i < Vcopy4.size(); ++i) {
		Vfin4.push_back(Prn2T* Vcopy4[i]);
	}
	for (int i = 0; i < Vcopy5.size(); ++i) {
		Vfin5.push_back(Prn3T* Vcopy5[i]);
	}
	for (int i = 0; i < Vcopy6.size(); ++i) {
		Vfin6.push_back(Prn4T* Vcopy6[i]);
	}
	for (int i = 0; i < Vcopy7.size(); ++i) {
		Vfin7.push_back(Brz3T* Vcopy7[i]);
	}
	for (int i = 0; i < Vcopy8.size(); ++i) {
		Vfin8.push_back(Brz4T* Vcopy8[i]);
	}
}

void Application::moveTo(float x1, float y1) {
	x = x1;
	y = y1;

}

void Application::LineTo(Vec3& b) {
	drawLine(x,y, b.x(), b.y());
	moveTo(b.x(), b.y());
}

void Application::LineTo(float a, float b) {
	drawLine(x, y, a, b);
	moveTo(a, b);
}

void Application::Triangles(Vec3 a, Vec3 b, Vec3 c){
	moveTo(a.x(), a.y());
	LineTo(b);
	LineTo(c);
	LineTo(a);
}

Vec3 Application::MidPoint(const Vec3 A, const Vec3 B) {
	Vec3 r;
	for (int i = 0; i < 3; ++i) {
		r.m[i] = (A.m[i] + B.m[i]) / 2;
	}
	return r;
}

void Application::Syerpinzky(Vec3 a, Vec3 b, Vec3 c, int subd) {

	if (subd >= 1) {
		Vec3 ab = MidPoint(a, b);
		Vec3 bc = MidPoint(b, c);
		Vec3 ca = MidPoint(c, a);
		Syerpinzky(a, ab, ca, subd - 1);
		Syerpinzky(ab, b, bc, subd - 1);
		Syerpinzky(ca, bc, c, subd - 1);
	}
	else {
		v.push_back(a);
		v.push_back(b);
		v.push_back(c);
	}

}

void Application::drawPolygon(int sides) {
	float angulo = 360 / sides;
	int x0, x1, y0, y1;
	int radio = 250;
	x0 = widthCenter + radio;
	y0 = heightCenter;
	for (int ang = 0; ang <= 360; ang += angulo) {
		x1 =radio * cos(ang*rad)+ widthCenter;
		y1 =radio * sin (ang * rad) + heightCenter;
		drawLine(x0, y0, x1, y1);
		x0 = x1;
		y0 = y1;
	}
}

void Application::drawGeometry(const std::vector<Vec3>& VT, Tipo tipo) {
	switch (tipo) {
	case TRIANGLEFAN:
		if (VT.size()>0) {
			for (int i = 1; i <= VT.size(); ++i) {
				moveTo(VT[0].m[0], VT[0].m[1]);
				LineTo(VT[i].m[0], VT[i].m[1]);
				if (i+1 < VT.size()) {
					LineTo(VT[i + 1].m[0], VT[i + 1].m[1]);
					
				}
				else {
					break;
				}
				LineTo(VT[0].m[0], VT[0].m[1]);
			}
		}
		break;
	case TRIANGLES:
		if (VT.size()>0) {
			for (int i = 0; i < VT.size(); i+=3) {
				moveTo(VT[i].m[0], VT[i].m[1]);
				if (i+1 <VT.size()&&i+2<VT.size()) {
					LineTo(VT[i + 1].m[0], VT[i + 1].m[1]);
					LineTo(VT[i + 2].m[0], VT[i + 2].m[1]);
					LineTo(VT[i].m[0], VT[i].m[1]);
				}
				else {
					break;
				}
			}
		}
		break;
	case LINES:
		if (VT.size()>0) {
			for (int i = 0; i < VT.size(); i+=2) {
				moveTo(VT[i].m[0], VT[i].m[1]);
				if (i+1<VT.size()) {
					LineTo(VT[i + 1].m[0], VT[i + 1].m[1]);
				}
				else {
					break;
				}
			}
		}
		break;
	case LINESTRIP:
		if (VT.size()>0) {
			for (int i = 0; i <= VT.size(); ++i) {
				moveTo(VT[i].m[0], VT[i].m[1]);
				if (i + 1< VT.size()) {
					LineTo(VT[i + 1].m[0], VT[i + 1].m[1]);
				}
				else {
					break;
				}
			}
		}
		break;
	case LINELOOP:
		if (VT.size()>0) {
			for (int i = 0; i <=VT.size(); ++i) {
				moveTo(VT[i].m[0], VT[i].m[1]);
				if (i + 1< VT.size()) {
					LineTo(VT[i + 1].m[0], VT[i + 1].m[1]);
				}
				else {
					LineTo(VT[0].m[0], VT[0].m[1]);
					break;
				}
			}
			LineTo(VT[0].m[0], VT[0].m[1]);
		}
		break;
	case POINTS:
		if (VT.size()>0) {
			for (int i = 0; i < VT.size(); i++) {
				putPixel(VT[i].m[0], VT[i].m[1]);
			}
		}
		break;
	case TRIANGLESTRIP:
		if (VT.size()>0) {
			for (int i = 2; i < VT.size(); ++i) {
				moveTo(VT[i].m[0], VT[i].m[1]);
				LineTo(VT[i-1].m[0], VT[i-1].m[1]);
				LineTo(VT[i-2].m[0], VT[i-2].m[1]);
				LineTo(VT[i].m[0], VT[i].m[1]);
			}
		}
		break;

	}
}

void Application::MidPointCircle(int x, int y, int rad) {
	float xP = 0;
	float yP = rad;
	float marg = 1-rad;

	while (xP<yP) {
			putPixel(x + xP, y + yP);
			putPixel(x + yP, y + xP);
			putPixel(x - yP, y + xP);
			putPixel(x - xP, y + yP);
			putPixel(x - xP, y - yP);
			putPixel(x - yP, y - xP);
			putPixel(x + yP, y - xP);
			putPixel(x + xP, y - yP);
		if (marg<0) {
			marg += (2*xP) + 3;
		}
		else {
			--yP;
			marg += (2*xP) - (2*yP) + 5;
		}
		++xP;
	}


	//while (xP >= yP){
	//	putPixel(x + xP, y + yP);
	//	putPixel(x + yP, y + xP);
	//	putPixel(x - yP, y + xP);
	//	putPixel(x - xP, y + yP);
	//	putPixel(x - xP, y - yP);
	//	putPixel(x - yP, y - xP);
	//	putPixel(x + yP, y - xP);
	//	putPixel(x + xP, y - yP);

	//	if (marg <= 0){
	//	  y += 1;
	//	  marg += 2 * y + 1;
	//	}
	//	else if (marg > 0){
	//	  x -= 1;
	//	  marg -= 2 * x + 1;
	//	}
	//}
}

void Application::draw() {
	//grad %= 360;
	//rotation += 2;
	//lados += (float)0.05;
	//if (rotation > 360) {
	//	rotation %= 360;
	//	lados -= 8;
	//}
	//drawLine(widthCenter, heightCenter,
	//	heightCenter + heightCenter * std::sin((rotation + grad) * radians),
	//	widthCenter + widthCenter * std::cos((rotation + grad) * radians)
	//);
	//drawPolygon(5);
	for (int i = 0; i < 1023; ++i) {
		for (int j = 0; j < 1023; ++j) {
			putPixel(i, j, 120, 100, 120, 255);
		}
	}

	//float radd = rand() % 100;
	//float xPri = rand() % 100;
	//float yPri = rand() % 100;

	/*MidPointCircle(xPri + 200, yPri +40, radd);
	MidPointCircle(xPri + 40, yPri + 70, radd);
	MidPointCircle(xPri + 180, yPri + 245, radd);
	MidPointCircle(xPri + 30, yPri + 89, radd);
	MidPointCircle(xPri + 2, yPri + 120, radd);
	MidPointCircle(xPri + 80, yPri + 263, radd);
	MidPointCircle(xPri + 27, yPri + 91, radd);
	MidPointCircle(xPri + 25, yPri + 5, radd);
	MidPointCircle(xPri + 100, yPri + 35, radd);
	MidPointCircle(xPri + 56, yPri + 63, radd);
	MidPointCircle(xPri + 140, yPri + 99, radd);
	MidPointCircle(xPri + 4, yPri + 42, radd);
	MidPointCircle(xPri + 120, yPri + 3, radd);
	MidPointCircle(xPri + 90, yPri + 269, radd);
	MidPointCircle(xPri + 10, yPri + 21, radd);*/

	//MidPointCircle(200+ widthCenter, 240 +heightCenter, 67);
	//MidPointCircle(340 + widthCenter, 70 + heightCenter, 46);
	//MidPointCircle(180 + widthCenter, 245 + heightCenter, 27);
	//MidPointCircle(30 + widthCenter, 89 + heightCenter, 100);
	//MidPointCircle(2 + widthCenter, 120 + heightCenter, 17);
	//MidPointCircle(180 + widthCenter, 263 + heightCenter, 46);
	//MidPointCircle(27 + widthCenter, 391 + heightCenter, 83);
	//MidPointCircle(25 + widthCenter, 15 + heightCenter, 52);
	//MidPointCircle(100 + widthCenter, 35 + heightCenter, 54);
	//MidPointCircle(56 + widthCenter, 163 + heightCenter, 60);
	//MidPointCircle(140 + widthCenter, 99 + heightCenter, 12);
	//MidPointCircle(4 + widthCenter, 242 + heightCenter, 29);
	//MidPointCircle(120 + widthCenter, 123 + heightCenter, 90);
	//MidPointCircle(390 + widthCenter, 269 + heightCenter, 21);
	//MidPointCircle(10 + widthCenter, 21 + heightCenter, 69);


	
	/*for (int i = 0; i < vc.size(); i += 3) {
		Triangles(vc[i], vc[i + 1], vc[i + 2]);
	
	}*/
	for (int i = 0; i < Vcopy.size(); i+=3) {
		moveTo(Vcopy[i].m[0], Vcopy[i].m[1]);
		LineTo(Vcopy[i + 1].m[0], Vcopy[i + 1].m[1]);
		LineTo(Vcopy[i + 2].m[0], Vcopy[i + 2].m[1]);
		LineTo(Vcopy[i].m[0], Vcopy[i].m[1]);

	}
	//for (int i = 0; i < Vcopy2.size(); i += 3) {
	//	moveTo(Vcopy2[i].m[0], Vcopy2[i].m[1]);
	//	LineTo(Vcopy2[i + 1].m[0], Vcopy2[i + 1].m[1]);
	//	LineTo(Vcopy2[i + 2].m[0], Vcopy2[i + 2].m[1]);
	//	LineTo(Vcopy2[i].m[0], Vcopy2[i].m[1]);

	//}
	/*for (int i = 0; i < Vcopy3.size(); i += 3) {
		moveTo(Vcopy3[i].m[0], Vcopy3[i].m[1]);
		LineTo(Vcopy3[i + 1].m[0], Vcopy3[i + 1].m[1]);
		LineTo(Vcopy3[i + 2].m[0], Vcopy3[i + 2].m[1]);
		LineTo(Vcopy3[i].m[0], Vcopy3[i].m[1]);

	}*/
	for (int i = 0; i < Vfin1.size(); i += 3) {
		moveTo(Vfin1[i].m[0], Vfin1[i].m[1]);
		LineTo(Vfin1[i + 1].m[0], Vfin1[i + 1].m[1]);
		LineTo(Vfin1[i + 2].m[0], Vfin1[i + 2].m[1]);
		LineTo(Vfin1[i].m[0], Vfin1[i].m[1]);

	}
	for (int i = 0; i < Vfin2.size(); i += 3) {
		moveTo(Vfin2[i].m[0], Vfin2[i].m[1]);
		LineTo(Vfin2[i + 1].m[0], Vfin2[i + 1].m[1]);
		LineTo(Vfin2[i + 2].m[0], Vfin2[i + 2].m[1]);
		LineTo(Vfin2[i].m[0], Vfin2[i].m[1]);

	}
	for (int i = 0; i < Vfin3.size(); i += 3) {
		moveTo(Vfin3[i].m[0], Vfin3[i].m[1]);
		LineTo(Vfin3[i + 1].m[0], Vfin3[i + 1].m[1]);
		LineTo(Vfin3[i + 2].m[0], Vfin3[i + 2].m[1]);
		LineTo(Vfin3[i].m[0], Vfin3[i].m[1]);

	}
	for (int i = 0; i < Vfin4.size(); i += 3) {
		moveTo(Vfin4[i].m[0], Vfin4[i].m[1]);
		LineTo(Vfin4[i + 1].m[0], Vfin4[i + 1].m[1]);
		LineTo(Vfin4[i + 2].m[0], Vfin4[i + 2].m[1]);
		LineTo(Vfin4[i].m[0], Vfin4[i].m[1]);

	}
	for (int i = 0; i < Vfin5.size(); i += 3) {
		moveTo(Vfin5[i].m[0], Vfin5[i].m[1]);
		LineTo(Vfin5[i + 1].m[0], Vfin5[i + 1].m[1]);
		LineTo(Vfin5[i + 2].m[0], Vfin5[i + 2].m[1]);
		LineTo(Vfin5[i].m[0], Vfin5[i].m[1]);

	}
	for (int i = 0; i < Vfin6.size(); i += 3) {
		moveTo(Vfin6[i].m[0], Vfin6[i].m[1]);
		LineTo(Vfin6[i + 1].m[0], Vfin6[i + 1].m[1]);
		LineTo(Vfin6[i + 2].m[0], Vfin6[i + 2].m[1]);
		LineTo(Vfin6[i].m[0], Vfin6[i].m[1]);

	}
	for (int i = 0; i < Vfin7.size(); i += 3) {
		moveTo(Vfin7[i].m[0], Vfin7[i].m[1]);
		LineTo(Vfin7[i + 1].m[0], Vfin7[i + 1].m[1]);
		LineTo(Vfin7[i + 2].m[0], Vfin7[i + 2].m[1]);
		LineTo(Vfin7[i].m[0], Vfin7[i].m[1]);

	}
	for (int i = 0; i < Vfin8.size(); i += 3) {
		moveTo(Vfin8[i].m[0], Vfin8[i].m[1]);
		LineTo(Vfin8[i + 1].m[0], Vfin8[i + 1].m[1]);
		LineTo(Vfin8[i + 2].m[0], Vfin8[i + 2].m[1]);
		LineTo(Vfin8[i].m[0], Vfin8[i].m[1]);

	}
	for (int i = 0; i < Vcopy9.size(); i += 3) {
		moveTo(Vcopy9[i].m[0], Vcopy9[i].m[1]);
		LineTo(Vcopy9[i + 1].m[0], Vcopy9[i + 1].m[1]);
		LineTo(Vcopy9[i + 2].m[0], Vcopy9[i + 2].m[1]);
		LineTo(Vcopy9[i].m[0], Vcopy9[i].m[1]);

	}

	//drawGeometry(Geom, TRIANGLEFAN);

}




