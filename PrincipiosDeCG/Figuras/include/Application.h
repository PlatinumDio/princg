#pragma once
#include "BaseApplication.h"
class Application :public BaseApplication
{
public:
	int heightCenter;
	int widthCenter;
	int grad = 0;
	int lados, radians, rotation;
	float rad = (float) 3.1416/180;
	void setup() override;
	void update() override;
	void draw() override;
	void drawPolygon(int sides);
};
