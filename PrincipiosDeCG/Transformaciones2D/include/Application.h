#pragma once
#include <vector>
#include "BaseApplication.h"
#include "Vec3.h"
//using namespace std;
class Application :public BaseApplication
{
public:
	float aang = 0, korra = 0 ;
	float x, y;
	std::vector<Vec3> v;
	std::vector<Vec3> vc;
	int heightCenter;
	int widthCenter;
	int grad = 0;
	int lados, radians, rotation;
	float rad = (float) 3.1416/180;
	void setup() override;
	void update() override;
	void draw() override;
	Vec3 MidPoint(Vec3 a, Vec3 b);
	void moveTo(float x1, float y1);
	void LineTo( Vec3& b);
	void Triangles(Vec3 a, Vec3 b, Vec3 c);
	void drawPolygon(int sides);
	void Syerpinzky(Vec3 a, Vec3 b, Vec3 c, int subd);
};
