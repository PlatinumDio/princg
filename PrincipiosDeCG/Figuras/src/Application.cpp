#include "stdafx.h"
#include "Application.h"
#include "BaseApplication.h"

void Application::setup() {
	heightCenter = HEIGHT / 2;
	widthCenter = WIDTH / 2;
}

void Application::update() {

}

void Application::drawPolygon(int sides) {
	float angulo = 360 / sides;
	int x0, x1, y0, y1;
	int radio = 250;
	x0 = widthCenter + radio;
	y0 = heightCenter;
	for (int ang = 0; ang <= 360; ang += angulo) {
		x1 =radio * cos(ang*rad)+ widthCenter;
		y1 =radio * sin (ang * rad) + heightCenter;
		drawLine(x0, y0, x1, y1);
		x0 = x1;
		y0 = y1;
	}
}

void Application::draw() {
	//using namespace std::chrono_literals;
	for (int i = 0; i < 1023; ++i) {
		for (int j = 0; j < 1023; ++j) {
			putPixel(i, j, 120, 120, 120, 255);
		}
	}
	
	grad %= 360;
	rotation += 2;
	lados += (float)0.05;
	if (rotation > 360) {
		rotation %= 360;
		lados -= 8;
	}
	drawLine(widthCenter, heightCenter,
		heightCenter + heightCenter * std::sin((rotation + grad) * radians),
		widthCenter + widthCenter * std::cos((rotation + grad) * radians)
	);
	drawPolygon(5);
	//std::this_thread::sleep_for(10ms);
}



