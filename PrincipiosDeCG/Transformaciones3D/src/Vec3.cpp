#include "Vec3.h"

Vec3::Vec3() {
	m[2] = 1;
}

Vec3::~Vec3() {

}

Vec3::Vec3(float x, float y, float v) {
	m[0] = x;
	m[1] = y;
	m[2] = v;
}

Vec3& Vec3::operator-(const Vec3& v)const {
	
	return Vec3(m[0]-v.m[0], m[1] - v.m[1], 1);

}

