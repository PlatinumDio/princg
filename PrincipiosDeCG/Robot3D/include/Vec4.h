
#ifndef VEC4_H
#define VEC4_H
class Vec4
{
public:
	float m[4];
	float x() { return m[0]; }
	float y() { return m[1]; }
	float z() { return m[2]; }
	Vec4(); //(0,0,0,1)
	~Vec4();
	Vec4(const float& x, const float& y, const float& z);
	//Vec4(const Vec4& v);
	Vec4 Normalize(); //normaliza el vector
	Vec4& Cross(const Vec4& c) const;//producto cruz
	float Dot(const Vec4& c) const;//producto punto
	Vec4& operator-(const Vec4& rh) const;

};
#endif

