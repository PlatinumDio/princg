#pragma once
#include "BaseApplication.h"
class Vec3; 
class Vec4;

using namespace std;
class Application :public BaseApplication
{
public:
	enum Tipo {
		TRIANGLES,
		POINTS,
		LINES,
		LINESTRIP,
		LINELOOP,
		TRIANGLESTRIP,
		TRIANGLEFAN
	};
	void drawGeometry(const std::vector<Vec3>& VT, Tipo tipo);
	float aang = 0, korra = 0 ;
	float x, y;
	std::vector<Vec3> Geom;
	std::vector<Vec3> v;
	std::vector<Vec3> vc;
	std::vector<Vec4> V3;
	std::vector<Vec4> Vcopy;
	int heightCenter;
	int widthCenter;
	int grad = 0;
	int lados, radians, rotation;
	float rad = (float) 3.1416/180;
	void setup() override;
	void update() override;
	void draw() override;
	Vec3 MidPoint(Vec3 a, Vec3 b);
	void moveTo(float x1, float y1);
	void LineTo( Vec3& b);
	void LineTo(float x, float y);

	//void Lines(vector<Vec3> a);
	//void Line_Strip(vector<Vec3> a);
	//void Line_Loop(vector<Vec3> a);
	//void Triangle_Fan(vector<Vec3> a);
	//void Triangle_String(vector<Vec3> a);

	void Triangles(Vec3 a, Vec3 b, Vec3 c);
	void drawPolygon(int sides);
	void Syerpinzky(Vec3 a, Vec3 b, Vec3 c, int subd);
};
